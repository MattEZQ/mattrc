map <leader>b :call JsBeautify()<cr>
" or
autocmd FileType javascript noremap <buffer>  <leader>b :call JsBeautify()<cr>
" for html
autocmd FileType html noremap <buffer> <leader>b :call HtmlBeautify()<cr>
" for css or scss
autocmd FileType css noremap <buffer> <leader>b :call CSSBeautify()<cr>
