" open NERDTree with leader-n
nnoremap <leader>n :NERDTreeToggle<CR>

" fix so NERDTree doesn't break on ascii terminals
let g:NERDTreeDirArrows=0
