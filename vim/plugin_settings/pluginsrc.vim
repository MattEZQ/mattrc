" NERDTree
Plugin 'scrooloose/NERDTree'
source ~/mattrc/vim/plugin_settings/nerdtree.vim

" NERDTree git plugin
" Plugin 'Xuyuanp/nerdtree-git-plugin'

" Ctrl-P
Plugin 'kien/ctrlp.vim'

" fugitive
Plugin 'tpope/vim-fugitive'
source ~/mattrc/vim/plugin_settings/fugitive.vim

" Gundo
Plugin 'vim-scripts/gundo'
source ~/mattrc/vim/plugin_settings/gundo.vim

" vim-airline
Plugin 'bling/vim-airline'

" tmuxline
Plugin 'edkolev/tmuxline.vim'

" vim-tmux
Plugin 'tmux-plugins/vim-tmux'

"vim-tmux-focux-events
Plugin 'tmux-plugins/vim-tmux-focus-events'

" promptline
Plugin 'edkolev/promptline.vim'

" base16
Plugin 'chriskempson/base16-vim'

" emmet
Plugin 'mattn/emmet-vim'
source ~/mattrc/vim/plugin_settings/emmet.vim
" vim-javascript
Plugin 'pangloss/vim-javascript'

" vim-jsbeautify - depends on nodejs
Plugin 'maksimr/vim-jsbeautify'
Plugin 'einars/js-beautify'
source ~/mattrc/vim/plugin_settings/vim-jsbeautify.vim

" jsHint
Plugin 'wookiehangover/jshint.vim'

"syntastic
Plugin 'scrooloose/syntastic'

" requires >=7.4, which isn't universally supported
" UltiSnips
" Plugin 'SirVer/UltiSnips'
" source ~/mattrc/vim/plugin_settings/ultisnips
""""""""""""""""""""
"Disabled below so i can learn one at a time
""""""""""""""""""""

"supertab
"Plugin 'ervandew/supertab'

"surround-vim
"Plugin 'tpope/vim-surround'

"NERD commenter
"Plugin 'scrooloose/nerdcommenter'

"vimux
"Plugin 'benmills/vimux'

