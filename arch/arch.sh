timedatectl set-ntp true

parted /dev/sdd
mklabel gpt
y
mkpart ESP fat32 1Mib 513Mib
set 1 boot on
mkpart primary btrfs 513Mib 100%
quit
mkfs.fat32 -F32 /dev/sdd1
mkfs.btrfs /dev/sdd1
mount /dev/sdd2 /mnt
mkdir /mnt/boot
mount /dev/sdd1 /mnt/boot
vim /etc/pacman.d/mirrorlist
pacstrap -i /mnt base base-devel
genfstab -U /mnt > /mnt/etc/fstab
arch-chroot /mnt/bin/bash
