vi /etc/locale.gen
locale-gen
echo LANG=en_US.UTF-8 > /etc/locale.conf
ln -sf /usr/share/zoneinfo/US/Central /etc/localtime
hwclock --systohc --utc
echo local-arch > /etc/hostname
vim /etc/hosts
systemctl enable dhcpcd@enp3s0
passwd
pacman -S dosfstools
bootctl --path=/boot install
vim /boot/loader/entries/arch.conf
vim /boot/loader/loader.conf
exit
umount -R /mnt
reboot
