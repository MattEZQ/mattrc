#!/bin/bash

#move to home directory and clone repo there
cd ~
rm -rf mattrc/
git clone https://github.com/matttelliott/mattrc ~/mattrc/

#install base16 colors
#git clone https://github.com/chriskempson/base16-shell.git ~/mattrc/base16-shell

#install language managers
#nvm /javascript
git clone https://github.com/creationix/nvm.git ~/.nvm && cd ~/.nvm && git checkout `git describe --abbrev=0 --tags`
source ~/.nvm/nvm.sh
nvm install stable

#rbenv
git clone https://github.com/sstephenson/rbenv.git ~/mattrc/rbenv
git clone https://github.com/sstephenson/ruby-build.git ~/mattrc/rbenv/plugins/ruby-build
rm -rf ~/.rbenv
ln -s ~/mattrc/rbenv ~/.rbenv
rbenv install 2.2.2
rbenv global 2.2.2

#phpenv
git clone https://github.com/CHH/phpenv.git ~/mattrc/phpenv
cd ~/mattrc/phpenv/bin/
rm -rf ~/.phpenv
./phpenv-install.sh

#install plugin managers
git clone https://github.com/gmarik/Vundle.vim ~/mattrc/vim/bundle/Vundle.vim
git clone https://github.com/tmux-plugins/tpm ~/mattrc/tmux/plugins/tpm

#remove config old files
#create symlinks to mattrc configs
rm -rf ~/.bashrc
ln -s ~/mattrc/bash/bashrc ~/.bashrc 
rm -rf ~/.profile
ln -s ~/mattrc/bash/profile ~/.profile 
rm -rf ~/.bash_profile
ln -s ~/mattrc/bash/bash_profile ~/.bash_profile 

rm -rf ~/.tmux.conf
ln -s ~/mattrc/tmux/.tmux.conf ~/.tmux.conf

rm -rf ~/.gitconfig
ln -s ~/mattrc/git/.gitconfig ~/.gitconfig
rm -rf ~/.editorconfig
ln -s ~/mattrc/.editorconfig ~/.editorconfig

#source files
source ~/.bashrc

#symlink irssi config
rm -rf ~/.irssi
ln -s ~/mattrc/irssi ~/.irssi

#install vim plugins
rm -rf ~/.vim
ln -s ~/mattrc/vim ~/.vim
rm -rf ~/.vimrc
ln -s ~/mattrc/vim/vimrc ~/.vimrc
vim +PluginInstall +qall
